
from App import db

class HtmlCssJs(db.Model):
    __tablename__ = 'ohrm_job_title'
    id        = db.Column(db.Integer, primary_key=True)
    job_title = db.Column(db.String(80))
    job_description = db.Column(db.String(80))

    def toJSON(self):
        json = ''
        job_title = "\"job_title\": \"%s\"" % self.job_title
        json += job_title + ","
        job_description = "\"job_description\": \"%s\"" % self.job_description
        json += job_description + ","
        id = "\"id\": \"%d\"" % self.id
        json += id
        return "{" + json  + "}"