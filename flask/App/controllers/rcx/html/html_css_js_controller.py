
from flask import jsonify
from App import app, db, render_template, request

@app.route('/rcx/html-css-js/<name>', methods=['GET'])
def index(name):
    return render_template('rcx/html/html_css_js.html')

@app.route('/rcx/html-css-js/list', methods=['GET'])
def list():
    return 'List '

@app.route('/rcx/html-css-js/<name>/add', methods=['GET'])
def add(name):
    return 'Add '

@app.route('/rcx/html-css-js/<name>/save', methods=['POST'])
def save(name):
    return 'Save '

@app.route('/rcx/html-css-js/<name>/load', methods=['GET'])
def load(name):
    return 'Load '

@app.route('/rcx/html-css-js/<name>/remove', methods=['GET'])
def remove(name):
    return 'Remove '