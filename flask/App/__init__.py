
import os
from flask import Flask, render_template, request
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, '../../_data/app.sqlite3')
db = SQLAlchemy(app)

import App.controllers.rcx.html.html_css_js_controller

#import App.models.admin.jobtitle
#db.create_all()