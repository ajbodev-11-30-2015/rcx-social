
package App.Models.Rcx.Html;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
//import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class HtmlCssJs {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    @Column(name = "name", unique = true, nullable = false)
    private String name;
    private String input__html;
    private String input__css;
    private String input__js;

    public HtmlCssJs() {}

    public long getId() { return this.id; }
    public String getName() { return this.name; }
    
    public void setName(String name) {
        this.name = name;
    }
    public void setInputHtml(String input__html) {
        this.input__html = input__html;
    }
    public void setInputCss(String input__css) {
        this.input__css = input__css;
    }
    public void setInputJs(String input__js) {
        this.input__js = input__js;
    }
    public String toJSON() {
        return String.format("{" + 
            "\"name\":\"%s\"," + 
            "\"input__html\":\"%s\"," + 
            "\"input__css\":\"%s\"," + 
            "\"input__js\":\"%s\"," + 
            "\"id\":%d}",
            name,input__html,input__css,input__js,id);
    }
}