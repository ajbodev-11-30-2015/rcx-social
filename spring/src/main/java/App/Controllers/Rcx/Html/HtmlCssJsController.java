
package App.Controllers.Rcx.Html;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import java.util.StringJoiner;
import com.google.gson.Gson;

import App.Repositories.Rcx.Html.HtmlCssJsRepository;
import App.Models.Rcx.Html.HtmlCssJs;

@Controller
//@RestController
@RequestMapping("/rcx/html-css-js")
public class HtmlCssJsController {

    @Autowired
    HtmlCssJsRepository repository;
    
    @RequestMapping(value="/list", method=RequestMethod.GET)
    public @ResponseBody String getList() {
        StringJoiner entities = new StringJoiner(",");
        for (HtmlCssJs entity : repository.findAll()) {
            entities.add(entity.toJSON());
        }
        return "[" + entities.toString() + "]";
    }

    @RequestMapping(value="/{name}", method=RequestMethod.GET)
    public String getIndex(@PathVariable String name, Model model) {
        model.addAttribute("title", "RCX : HTML CSS JS");
        model.addAttribute("name", name);
        return "rcx/html/index";
    }

    @RequestMapping(value="/{name}/save", method=RequestMethod.POST)
    public @ResponseBody String postSave(
        String input__html,
        String input__css,
        String input__js,
        @PathVariable String name
        ) {
        HtmlCssJs entity = repository.findByName(name).get(0);
        entity.setInputHtml(input__html);
        entity.setInputCss(input__css);
        entity.setInputJs(input__js);
        repository.save(entity);
        return "Save " + name;
    }

    @RequestMapping(value="/{name}/add", method=RequestMethod.GET)
    public @ResponseBody String getAdd(@PathVariable String name) {
        HtmlCssJs entity = new HtmlCssJs();
        entity.setName(name);
        repository.save(entity);
        return "Add " + name;
    }

    @RequestMapping(value="/{name}/load", method=RequestMethod.GET)
    public @ResponseBody String getLoad(@PathVariable String name) {
        HtmlCssJs entity = repository.findByName(name).get(0);
        Gson gson = new Gson();
        return gson.toJson(entity);
    }

    @RequestMapping(value="/{name}/remove", method=RequestMethod.GET)
    public @ResponseBody String getRemove(@PathVariable String name) {
        for (HtmlCssJs entity : repository.findByName(name)) {
            repository.delete(entity.getId());
        }
        return "Remove " + name;
    }

}