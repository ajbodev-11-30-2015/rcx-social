package App.Repositories.Rcx.Html;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import App.Models.Rcx.Html.HtmlCssJs;

public interface HtmlCssJsRepository extends CrudRepository<HtmlCssJs, Long> {

    List<HtmlCssJs> findByName(String name);
    //Long deleteByName(String name);
    //List<HtmlCssJs> removeByName(String name);
}
