
class Rcx::Html::HtmlCssJsController < ApplicationController
  layout "rcx"
  def index
    render file: 'rcx/html/html_css_js.html.erb'
  end
  def list
    render :text => 'List '
  end
  def save
    render :text => 'Save '
  end
  def add
    render :text => 'Added '
  end
  def load
    render :text => 'Load '
  end
  def remove
    render :text => 'Remove '
  end
end