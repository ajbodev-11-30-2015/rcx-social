
ActiveRecord::Schema.define() do
  create_table 'ohrm_job_title', force: true do |t|
    t.string 'job_title'
    t.string 'job_description'
  end
end