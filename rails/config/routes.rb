RubyRor::Application.routes.draw do
    
  get  ':controller/:name(/:action)', constraints: { action: /index|list|add|load|remove/ }
  post ':controller/:name/:action', constraints: { action: /save/ }
  
  match 'rcx/html-css-js/:name(/:action)', to: 'rcx/html/html_css_js', via: :all
  
end