
(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-6\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__ls\" data-toggle=\"tab\">LiveScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__ls\" class=\"tab-pane active\"><textarea id=\"input__ls\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__ls()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__ls&quot;)\"/></span>&nbsp<br/><br/></div></div></div><div class=\"col-sm-6\"><div class=\"nav-tabs-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__js\" data-toggle=\"tab\">JavaScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__js\" class=\"tab-pane active\"><textarea id=\"output__js\"></textarea><br/><button type=\"button\" onclick=\"app.exports.output__js()\" class=\"btn\">&#8595; Export</button>&nbsp<input id=\"lint__output__js\" type=\"checkbox\" onchange=\"app.lints.output__js()\" checked=\"checked\"/>&nbspLint<br/><br/></div></div></div></div>");;return buf.join("");
}dmt["layout-default"] = template(); })();

(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-12\"><div class=\"col-sm-2\"><ul class=\"nav nav-tabs tabs-left\"><li class=\"active\"><a href=\"#tab__input\" data-toggle=\"tab\">Input</a></li><li><a href=\"#tab__output\" data-toggle=\"tab\">Output</a></li></ul></div><div class=\"col-sm-10\"><div class=\"tab-content\"><div id=\"tab__input\" class=\"tab-pane active\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__ls\" data-toggle=\"tab\">LiveScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__ls\" class=\"tab-pane active\"><textarea id=\"input__ls\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__ls()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__ls&quot;)\"/></span>&nbsp<br/><br/></div></div></div><div id=\"tab__output\" class=\"tab-pane\"><div class=\"nav-tabs-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__js\" data-toggle=\"tab\">JavaScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__js\" class=\"tab-pane active\"><textarea id=\"output__js\"></textarea><br/><button type=\"button\" onclick=\"app.exports.output__js()\" class=\"btn\">&#8595; Export</button>&nbsp<input id=\"lint__output__js\" type=\"checkbox\" onchange=\"app.lints.output__js()\" checked=\"checked\"/>&nbspLint<br/><br/></div></div></div></div></div></div></div>");;return buf.join("");
}dmt["layout-alt"] = template(); })();
