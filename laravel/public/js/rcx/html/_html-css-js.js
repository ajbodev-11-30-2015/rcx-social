
/*
// HTML
(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<!DOCTYPE html>\n<html>\n  <head>\n  </head>\n  <body>\n    <ul>\n      <li><a href=\"#/\">Home</a></li>\n      <li><a href=\"#/som\">Som</a></li>\n    </ul>\n    <div id='app'>\n    </div>\n  </body>\n</html>");;return buf.join("");
}dmt["input__html"] = template(); })();

// CSS
(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("#app {\n  background: red;\n  height: 50px;\n  width: 50px;\n} ");;return buf.join("");
}dmt["input__css"] = template(); })();

// JS
(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("");;return buf.join("");
}dmt["input__js"] = template(); })();
*/

(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-6\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__html\" data-toggle=\"tab\">HTML</a></li><li><a href=\"#tab__input__css\" data-toggle=\"tab\">CSS</a></li><li><a href=\"#tab__input__js\" data-toggle=\"tab\">JavaScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__html\" class=\"tab-pane active\"> <textarea id=\"input__html\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__html()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__html&quot;)\"/></span></div><div id=\"tab__input__css\" class=\"tab-pane\"><textarea id=\"input__css\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__css()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__css&quot;)\"/></span>&nbsp<input id=\"lint__input__css\" type=\"checkbox\" onchange=\"app.lints.input__css()\" checked=\"checked\"/>&nbspLint</div><div id=\"tab__input__js\" class=\"tab-pane\"><textarea id=\"input__js\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__js()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__js&quot;)\"/></span>&nbsp<input id=\"lint__input__js\" type=\"checkbox\" onchange=\"app.lints.input__js()\" checked=\"checked\"/>&nbspLint</div><br/><br/></div></div><div class=\"col-sm-6\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__iframe\" data-toggle=\"tab\">Iframe</a></li><li><a href=\"#tab__output__error\" data-toggle=\"tab\">Error</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__iframe\" class=\"tab-pane active\"><iframe id=\"output__iframe\"></iframe><br/><br/><button type=\"button\" onclick=\"app.exports.output__iframe()\" class=\"btn\">&#8595; Export</button><br/><br/></div><div id=\"tab__output__error\" class=\"tab-pane\"><div id=\"output__error\"> </div></div></div></div>");;return buf.join("");
}dmt["layout-default"] = template(); })();

(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-12\"><div class=\"col-sm-2\"><ul class=\"nav nav-tabs tabs-left\"><li class=\"active\"><a href=\"#tab__input\" data-toggle=\"tab\">Input</a></li><li><a href=\"#tab__output\" data-toggle=\"tab\">Output</a></li></ul></div><div class=\"col-sm-10\"><div class=\"tab-content\"><div id=\"tab__input\" class=\"tab-pane active\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__html\" data-toggle=\"tab\">HTML</a></li><li><a href=\"#tab__input__css\" data-toggle=\"tab\">CSS</a></li><li><a href=\"#tab__input__js\" data-toggle=\"tab\">JavaScript</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__html\" class=\"tab-pane active\"> <textarea id=\"input__html\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__html()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__html&quot;)\"/></span></div><div id=\"tab__input__css\" class=\"tab-pane\"><textarea id=\"input__css\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__css()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__css&quot;)\"/></span>&nbsp<input id=\"lint__input__css\" type=\"checkbox\" onchange=\"app.lints.input__css()\" checked=\"checked\"/>&nbspLint</div><div id=\"tab__input__js\" class=\"tab-pane\"><textarea id=\"input__js\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__js()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__js&quot;)\"/></span>&nbsp<input id=\"lint__input__js\" type=\"checkbox\" onchange=\"app.lints.input__js()\" checked=\"checked\"/>&nbspLint</div><br/><br/></div></div><div id=\"tab__output\" class=\"tab-pane\"><div class=\"nav-tabs-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__iframe\" data-toggle=\"tab\">Iframe</a></li><li><a href=\"#tab__output__error\" data-toggle=\"tab\">Error</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__iframe\" class=\"tab-pane active\"><iframe id=\"output__iframe\"></iframe><br/><br/><button type=\"button\" onclick=\"app.exports.output__iframe()\" class=\"btn\">&#8595; Export</button><br/><br/></div><div id=\"tab__output__error\" class=\"tab-pane\"><div id=\"output__error\"> </div></div></div></div></div></div></div></div>");;return buf.join("");
}dmt["layout-alt"] = template(); })();
