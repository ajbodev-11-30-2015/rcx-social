
(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-6\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__jade\" data-toggle=\"tab\">Jade</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__jade\" class=\"tab-pane active\"><textarea id=\"input__jade\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__jade()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__jade&quot;)\"/></span></div><br/><br/></div></div><div class=\"col-sm-6\"><div class=\"nav-tabs-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__html\" data-toggle=\"tab\">HTML</a></li><li><a href=\"#tab__output__iframe\" data-toggle=\"tab\">Iframe</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__html\" class=\"tab-pane active\"><textarea id=\"output__html\"></textarea><br/><button type=\"button\" onclick=\"app.exports.output__html()\" class=\"btn\">&#8595; Export</button></div><div id=\"tab__output__iframe\" class=\"tab-pane\"><iframe id=\"output__iframe\"></iframe></div></div></div></div>");;return buf.join("");
}dmt["layout-default"] = template(); })();

(function() {function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"col-sm-12\"><div class=\"col-sm-2\"><ul class=\"nav nav-tabs tabs-left\"><li class=\"active\"><a href=\"#tab__input\" data-toggle=\"tab\">Input</a></li><li><a href=\"#tab__output\" data-toggle=\"tab\">Output</a></li></ul></div><div class=\"col-sm-10\"><div class=\"tab-content\"><div id=\"tab__input\" class=\"tab-pane active\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__input__jade\" data-toggle=\"tab\">Jade</a></li></ul><div class=\"tab-content\"><div id=\"tab__input__jade\" class=\"tab-pane active\"><textarea id=\"input__jade\"></textarea><br/><button type=\"button\" onclick=\"app.exports.input__jade()\" class=\"btn\">&#8595; Export</button>&nbsp&nbsp<span class=\"btn btn-default btn-file\">&#8593; Import<input type=\"file\" onchange=\"app.importer(this, &quot;input__jade&quot;)\"/></span></div><br/><br/></div></div><div id=\"tab__output\" class=\"tab-pane\"><div class=\"nav-tabs-custom\"><ul class=\"nav nav-tabs\"><li class=\"active\"><a href=\"#tab__output__html\" data-toggle=\"tab\">HTML</a></li><li><a href=\"#tab__output__iframe\" data-toggle=\"tab\">Iframe</a></li></ul><div class=\"tab-content\"><div id=\"tab__output__html\" class=\"tab-pane active\"><textarea id=\"output__html\"></textarea><br/><button type=\"button\" onclick=\"app.exports.output__html()\" class=\"btn\">&#8595; Export</button></div><div id=\"tab__output__iframe\" class=\"tab-pane\"><iframe id=\"output__iframe\"></iframe></div></div></div></div></div></div></div>");;return buf.join("");
}dmt["layout-alt"] = template(); })();
