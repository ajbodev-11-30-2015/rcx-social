<?php 

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Request;
use App\Entity\App\Time;

class RcxController extends Controller {
    
	public function __construct()
	{
		#$this->middleware('auth');
	}

	public function getIndex($rcx, $name)
	{
        $_rcx = str_replace('-', '_', $rcx);
        $dir = $this->dirs[$rcx];
        if ($name === 'list') {
            $rcxs = DB::connection('rcx')->table('rcx_'.$_rcx)->get();
            return response(json_encode($rcxs))->header('Content-Type', 'application/json');
        }
        else {
            $rcx2 = DB::connection('rcx')->table('rcx_'.$_rcx)->where('name',$name)->first();
            return view('rcx.'.$dir.'.'.$rcx)->with('rcx', $rcx2);
        }
	}

	public function postSave($rcx, $name)
	{
        $_rcx = str_replace('-', '_', $rcx);
        $_inputs = [];
        foreach ($this->inputs[$rcx] as $input) {
            $_inputs[$input] = Request::input($input);   
        }
        DB::connection('rcx')
            ->table('rcx_'.$_rcx)
            ->where('name',$name)
            ->update($_inputs);
        return var_dump($_inputs);
	}

	public function getLoad($rcx, $name)
	{
        $_rcx = str_replace('-', '_', $rcx);
        $rcx2 = DB::connection('rcx')
            ->table('rcx_'.$_rcx)
            ->where('name',$name)
            ->first();
        return json_encode($rcx2);
	}

	public function getAdd($rcx, $name)
	{
        $_rcx = str_replace('-', '_', $rcx);
        DB::connection('rcx')
            ->table('rcx_'.$_rcx)
            ->insert(['name' => $name]);
        return 'Added '.$name;
	}

	public function getRemove($rcx, $name)
	{
        $_rcx = str_replace('-', '_', $rcx);
        DB::connection('rcx')
            ->table('rcx_'.$_rcx)
            ->where('name',$name)
            ->delete();
        return 'Deleted '.$name;
	}

    private $dirs = [
        'html-css-js' => 'html',
        'jade'        => 'html',
        'markdown'    => 'html',
        'livescript'  => 'javascript',
    ];

    private $inputs = [
        'html-css-js' => ['input__html', 'input__css', 'input__js'],
        'jade'        => ['input__jade', 'output__html'],
        'markdown'    => ['input__md', 'output__html'],
        'livescript'  => ['input__ls', 'output__js'],
    ];

}
