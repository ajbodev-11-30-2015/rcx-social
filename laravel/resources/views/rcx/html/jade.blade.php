
@extends('rcx.main')

@section('App_Title', 'RCX : Jade')

@section('App_Content')
  <div class="row">
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="run__manual" type="button" onclick="app.runs.input__jade()" class="btn btn-success">&#x25BA; Run</button>&nbsp
        <input id="run__auto" type="checkbox" checked>&nbspAutorun
      </div>
    </div>
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="save__manual" type="button" onclick="app.save()" class="btn btn-primary">&#128190; Save</button>&nbsp
        <input id="save__auto" type="checkbox">&nbspAutosave
      </div>
    </div>
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="load" type="button" onclick="app.load()" class="btn btn-warning">&#x21bb; Load</button>&nbsp
        <input id="load__auto" type="checkbox">&nbspAutoload
      </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-6">
      <div class="nav">
        <div id="alt__text" style="float: right;">&nbspAlternative Layout</div>
        <input id="alt__layout" type="checkbox" style="float: right;" onchange="app.altLayout()">
      </div>
    </div>
  </div><br>
  <div id="app__layout" class="row">
    <div class="col-sm-6">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab__input__jade" data-toggle="tab">Jade</a></li>
              </ul>
              <div class="tab-content">
                <div id="tab__input__jade" class="tab-pane active">
                  <textarea id="input__jade"></textarea><br>
                  <button type="button" onclick="app.exports.input__jade()" class="btn">&#8595; Export</button>&nbsp&nbsp<span class="btn btn-default btn-file">&#8593; Import
                    <input type="file" onchange="app.importer(this, &quot;input__jade&quot;)"></span>
                </div><br><br>
              </div>
    </div>
    <div class="col-sm-6">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab__output__html" data-toggle="tab">HTML</a></li>
                  <li><a href="#tab__output__iframe" data-toggle="tab">Iframe</a></li>
                </ul>
                <div class="tab-content">
                  <div id="tab__output__html" class="tab-pane active">
                    <textarea id="output__html"></textarea><br>
                    <button type="button" onclick="app.exports.output__html()" class="btn">&#8595; Export</button>
                  </div>
                  <div id="tab__output__iframe" class="tab-pane">
                    <iframe id="output__iframe"></iframe>
                  </div>
                </div>
              </div>
    </div>
  </div>
@endsection

@section('App_Script')
  <script src="/js/_vendor/rcx/_codemirror/jade.js"></script>
  <script src="/js/_vendor/rcx/html/jade.js"></script>
  <script src="/js/rcx/html/_jade.js"></script>
  <script src="/js/rcx/html/jade.js"></script>
@endsection