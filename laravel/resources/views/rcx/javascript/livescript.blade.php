
@extends('rcx.main')

@section('App_Title', 'RCX : LiveScript')

@section('App_Content')
  <div class="row">
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="run__manual" type="button" onclick="app.runs.input__ls()" class="btn btn-success">&#x25BA; Run</button>&nbsp
        <input id="run__auto" type="checkbox" checked>&nbspAutorun
      </div>
    </div>
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="save__manual" type="button" onclick="app.save()" class="btn btn-primary">&#128190; Save</button>&nbsp
        <input id="save__auto" type="checkbox">&nbspAutosave
      </div>
    </div>
    <div class="col-xs-4 col-sm-3 col-md-2">
      <div class="nav">
        <button id="load" type="button" onclick="app.load()" class="btn btn-warning">&#x21bb; Load</button>&nbsp
        <input id="load__auto" type="checkbox">&nbspAutoload
      </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-6">
      <div class="nav">
        <div id="alt__text" style="float: right;">&nbspAlternative Layout</div>
        <input id="alt__layout" type="checkbox" style="float: right;" onchange="app.altLayout()">
      </div>
    </div>
  </div><br>
  <div id="app__layout" class="row">
    <div class="col-sm-6">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab__input__ls" data-toggle="tab">LiveScript</a></li>
              </ul>
              <div class="tab-content">
                <div id="tab__input__ls" class="tab-pane active">
                  <textarea id="input__ls"></textarea><br>
                  <button type="button" onclick="app.exports.input__ls()" class="btn">&#8595; Export</button>&nbsp&nbsp<span class="btn btn-default btn-file">&#8593; Import
                    <input type="file" onchange="app.importer(this, &quot;input__ls&quot;)"></span>&nbsp<br><br>
                </div>
              </div>
    </div>
    <div class="col-sm-6">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab__output__js" data-toggle="tab">JavaScript</a></li>
                </ul>
                <div class="tab-content">
                  <div id="tab__output__js" class="tab-pane active">
                    <textarea id="output__js"></textarea><br>
                    <button type="button" onclick="app.exports.output__js()" class="btn">&#8595; Export</button>&nbsp
                    <input id="lint__output__js" type="checkbox" onchange="app.lints.output__js()" checked>&nbspLint<br><br>
                  </div>
                </div>
              </div>
    </div>
  </div>
@endsection

@section('App_Script')
  <script src="/js/_vendor/rcx/_codemirror/lint.js"></script>
  <script src="/js/_vendor/rcx/_codemirror/javascript-lint.js"></script>
  <script src="/js/_vendor/rcx/jshint.js"></script>
  <script src="/js/_vendor/rcx/_codemirror/livescript.js"></script>
  <script src="/js/_vendor/rcx/javascript/livescript-min.js"></script>
  <script src="/js/rcx/javascript/_livescript.js"></script>
  <script src="/js/rcx/javascript/livescript.js"></script>
@endsection