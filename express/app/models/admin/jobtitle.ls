
o = {}

db = use 'db'

o = db.schema.define 'ohrm_job_title', do
  job_title:        String
  job_description:  String

if typeof module == 'object' then module.exports = o