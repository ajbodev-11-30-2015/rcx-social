
o = {}

o.route = 'rcx/html-css-js'

o.get = 
  'list': (req, res) !->
    res.send 'List '
  ':name': (req, res) !->
    res.render 'rcx/html/html-css-js'
    #res.send 'Index '
  ':name/add': (req, res) !->
    res.send 'Add '
  ':name/load': (req, res) !->
    res.send 'Load '
  ':name/remove': (req, res) !->
    res.send 'Remove '
    
o.post = 
  'save': (req, res) !->
    res.send 'Save '

if typeof module == 'object' then module.exports = o