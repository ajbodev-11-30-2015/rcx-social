﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using App.DAL;

namespace App.Controllers
{
    public class RcxController : Controller
    {
        private AppContext db = new AppContext();

        [HttpGet]
        public ActionResult Index(string rcx, string name)
        {
            if (name == "list") {
                return Content("List " + rcx);
            }
            else { 
                return Content("Index " + rcx + " " + name);
            }
        }

        [HttpPost]
        public ActionResult Save(string rcx, string name)
        {
            return Content("Save " + rcx + " " + name);
        }

        [HttpGet]
        public ActionResult Load(string rcx, string name)
        {
            return Content("Load " + rcx + " " + name);
        }

        [HttpGet]
        public ActionResult Add(string rcx, string name)
        {
            return Content("Add " + rcx + " " + name);
        }

        [HttpGet]
        public ActionResult Remove(string rcx, string name)
        {
            return Content("Remove " + rcx + " " + name);
        }
    }
}