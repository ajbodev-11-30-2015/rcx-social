﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using App.DAL;
using App.Models.Rcx.Html;

namespace App.Controllers.Rcx.Html
{
    public class HtmlCssJsController : Controller
    {
        private AppContext db = new AppContext();

        [HttpGet]
        public ActionResult Index(string name)
        {
            if (name == "list") {
                Response.ContentType = "application/json";
                return Content(JsonConvert.SerializeObject(db.HtmlCssJss));
            }
            else {
                var entity = db.HtmlCssJss.Where(o => o.name == name).First();
                ViewBag.Name = entity.name;
                return View("~/Views/Rcx/Html/HtmlCssJs.cshtml");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(string name)
        {
            var entity = db.HtmlCssJss.Where(o => o.name == name).First();
            entity.input__html = Request["input__html"];
            entity.input__css  = Request["input__css"];
            entity.input__js   = Request["input__js"];
            db.SaveChanges();
            return Content("HCJ Save " + name);
        }

        [HttpGet]
        public ActionResult Load(string name)
        {
            var entity = db.HtmlCssJss.Where(o => o.name == name).First();
            return Content(JsonConvert.SerializeObject(entity));
        }

        [HttpGet]
        public ActionResult Add(string name)
        {
            HtmlCssJs entity = new HtmlCssJs();
            entity.name = name;
            db.HtmlCssJss.Add(entity);
            db.SaveChanges();
            return Content("HCJ Add " + name);
        }

        [HttpGet]
        public ActionResult Remove(string name)
        {
            var entity = db.HtmlCssJss.Where(o => o.name == name).First();
            db.HtmlCssJss.Remove(entity);
            db.SaveChanges();
            return Content("HCJ Remove " + name);
        }
    }
}