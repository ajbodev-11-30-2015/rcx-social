﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

using App.Models.Rcx.Html;

namespace App.DAL
{
    public class AppContext : DbContext
    {
        public AppContext() : base("AppContext") { }

        public DbSet<HtmlCssJs> HtmlCssJss { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<HtmlCssJs>().ToTable("rcx_html_css_js"); 
        }
    }
}