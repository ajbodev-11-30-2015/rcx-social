﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Models.Rcx.Html
{
    public class HtmlCssJs
    {
        public int id { get; set; }

        public string name { get; set; }

        public string input__html { get; set; }

        public string input__css { get; set; }

        public string input__js { get; set; }

    }
}