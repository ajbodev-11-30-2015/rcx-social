﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace App
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*
            routes.MapRoute(
                name: "RCX",
                url: "rcx/{rcx}/{name}/{action}",
                defaults: new { controller = "Rcx", action = "Index", name=""}
            );
            */

            routes.MapRoute(
                name: "Default",
                url: "rcx/{controller}/{name}/{action}",
                defaults: new { controller = "Rcx", action = "Index", name = "" }
            );
        }
    }
}
